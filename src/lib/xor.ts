import {Middleware} from "@arabesque/core";

/**
 * Create a "XOR" middleware.
 *
 * https://en.wikipedia.org/wiki/XOR_gate
 */
export const createXORMiddleware = <Context>(...middlewares: Array<Middleware<Context>>): Middleware<Context> => {
    return (context, next) => {
        let numberOfOpenedMiddlewares: number = 0;

        const invokeMiddlewares = (context: any, middlewares: Array<Middleware<Context>>): Promise<Context> => {
            const middleware = middlewares[0];

            if (!middleware) {
                return Promise.resolve(context);
            }

            return middleware(context, (context) => {
                numberOfOpenedMiddlewares++;

                return Promise.resolve(context);
            }).then((context) => {
                return numberOfOpenedMiddlewares > 1 ? Promise.resolve(context) : invokeMiddlewares(context, middlewares.slice(1));
            });
        };

        return invokeMiddlewares(context, middlewares).then((context) => {
            const opened = numberOfOpenedMiddlewares === 1;

            return opened ? next(context) : Promise.resolve(context);
        });
    };
}