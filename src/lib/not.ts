import {Middleware} from "@arabesque/core";

/**
 * Create a "NOT" middleware.
 *
 * https://en.wikipedia.org/wiki/NOT_gate
 */
export const createNOTMiddleware = <Context>(middleware: Middleware<Context>): Middleware<Context> => {
    return (context, next) => {
        let opened: boolean = true;

        return middleware(context, (context) => {
            opened = false;

            return Promise.resolve(context);
        }).then((context) => {
            return opened ? next(context) : Promise.resolve(context);
        });
    };
}