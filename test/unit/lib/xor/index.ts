import * as tape from "tape";
import {Middleware} from "@arabesque/core";
import {spy} from "sinon";
import {createXORMiddleware} from "../../../../src";

const createInputMiddleware = (input: boolean): Middleware<any> => {
    return (context, next) => {
        return input ? next(context) : Promise.resolve(context);
    };
};

tape('XOR', ({test}) => {
    const output = spy((context, next) => {
        return next(context);
    });

    const testCases: Array<{
        inputs: Array<boolean>;
        expectation: boolean
    }> = [
        {
            inputs: [false],
            expectation: false
        },
        {
            inputs: [false, false],
            expectation: false
        },
        {
            inputs: [false, true],
            expectation: true
        },
        {
            inputs: [true],
            expectation: true
        },
        {
            inputs: [true, false],
            expectation: true
        },
        {
            inputs: [true, true],
            expectation: false
        }
    ];

    for (let {inputs, expectation} of testCases) {
        const middlewares = inputs.map(createInputMiddleware) as [Middleware<any>, ...Array<Middleware<any>>];

        const candidate = createXORMiddleware(...middlewares);

        test(`${inputs} -> ${expectation}`, ({end, same}) => {
            return candidate('foo', (context) => {
                return output(context, () => Promise.resolve());
            }).then(() => {
                same(output.callCount, expectation ? 1 : 0);

                output.resetHistory();

                end();
            });
        });
    }

    test('is lazy', ({end, same}) => {
        const firstMiddleware = spy(createInputMiddleware(true));

        const secondMiddleware = spy(createInputMiddleware(true));

        const thirdMiddleware = spy(createInputMiddleware(false));

        const candidate = createXORMiddleware(firstMiddleware, secondMiddleware, thirdMiddleware);

        return candidate('foo', () => Promise.resolve()).then(() => {
            same(firstMiddleware.callCount, 1, 'should execute first middleware');
            same(secondMiddleware.callCount, 1, 'should execute second middleware');
            same(thirdMiddleware.callCount, 0, 'should not execute third middleware');

            output.resetHistory();

            end();
        });
    });
});